# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2023 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

# Apply standard settings of oer-reveal.
#+INCLUDE: "~/.emacs.d/oer-reveal-org/config.org"

# Enable klipse, but disable scaling, which interferes.
# (This is only necessary for presentations using klipse.  Otherwise,
# the following three lines and subsequent klipse settings can be removed.)
#+OPTIONS: reveal_klipsify_src:t
#+REVEAL_MIN_SCALE: 1.0
#+REVEAL_MAX_SCALE: 1.0

# Host klipse files locally.
# The following line does not work, see: https://github.com/viebel/klipse/issues/360
# #+REVEAL_KLIPSE_EXTRA_CONFIG: window.klipse_settings.no_dynamic_scripts=true;
#+REVEAL_KLIPSE_SETUP: (("html" "selector_eval_html" "language-klipse-html") ("javascript" "selector_eval_js" "language-klipse-javascript") ("js" "selector_eval_js" "language-klipse-js") ("python" "selector_eval_python_client" "language-klipse-python"))
#+REVEAL_KLIPSE_JS_URL: ./reveal.js/plugin/klipse/klipse_plugin.js
#+REVEAL_KLIPSE_CSS_URL: ./reveal.js/plugin/klipse/codemirror.css
#+REVEAL_CODEMIRROR_CONFIG: scripts_root: './reveal.js/plugin/klipse',
#+REVEAL_CODEMIRROR_CONFIG: codemirror_root: './reveal.js/plugin/klipse'

# Set autoplay to true for audio plugin.
#+OER_REVEAL_AUDIO_SLIDESHOW_CONFIG: audioStartAtFragment: true, audio: { advance: -1, autoplay: true, defaultDuration: 0, defaultAudios: false, defaultAudioRate: window.location.search.match( /audio-speed/gi )? parseFloat((new URL(window.location.href)).searchParams.get('audio-speed')) : 1.0, playerOpacity: 0.8, playerStyle: 'position: fixed; bottom: 9.5vh; left: 0%; width: 30%; height:30px; z-index: 33;' }

# Show legalese.
#+REVEAL_PREAMBLE: <div class="legalese"><p><a href="/imprint.html">Imprint</a> | <a href="/privacy.html">Privacy Policy</a></p></div>

# Use custom title slide.
#+REVEAL_TITLE_SLIDE: title-slide.html

# For citations with the syntax of Org mode 9.5, elisp/publish.el sets
# emacs-reveal-cite-pkg to org-re-reveal-citeproc. (By default,
# emacs-reveal uses org-re-reveal-ref.)  Then, the following lines
# declare a BibTeX bibliography with citation style:
#+bibliography: references.bib
#+cite_export: csl

# See that commit which adapted this howto from org-re-reveal-ref to
# org-re-reveal-citeproc:
# https://gitlab.com/oer/emacs-reveal-howto/-/commit/51056ab07290d8b9e54a537246660a65e2f809dd

#+TITLE: How to create presentations with emacs-reveal
#+AUTHOR: Jens Lechtenbörger
#+DATE: August 2023 (emacs-reveal 9.18.1 and later)

# Define some metadata that is exported into the HTML presentation.
# It may or may not support search in repositories and by search engines:
#+KEYWORDS: emacs-reveal, howto, HTML, reveal.js, org, Org mode, presentation, slideshow, OER, open educational resource, licensing
#+DESCRIPTION: Howto for the creation of OER presentations (HTML with audio, figures, quizees, references to bibliography, and machine-readable licensing information) with reveal.js from Org mode sources using GNU Emacs

* Presentation Hints
** General
- My name is tomyli
   - Haha This is a [[https://revealjs.com][reveal.js]] presentation and an
     [[https://en.wikipedia.org/wiki/Open_educational_resources][Open Educational Resource (OER)]]
     - Generated with [[https://gitlab.com/oer/emacs-reveal][emacs-reveal]]
       from [[https://gitlab.com/oer/emacs-reveal-howto][Free/libre Org mode sources]]
       - See [[https://oer.gitlab.io/hints.html][usage hints for emacs-reveal presentations]]
     - Key bindings and navigation
       - Press “?” to see key bindings of reveal.js
         - In general, “n” and “p” move to next and previous slide; mouse
	   wheel works as well
         - Search with Ctrl-Shift-F
       - Up/down (swiping, arrows) move within sections,
         left/right jump between sections (type “o” to see what is where)
       - Type slide’s number followed by Enter to jump to that slide
       - Browser history
       - Zoom with Ctrl-Mouse or Alt-Mouse

** Why?
   - I created emacs-reveal as software bundle to produce
     [[https://en.wikipedia.org/wiki/Open_educational_resources][Open Educational Resources (OER)]]
     for my own teaching
     - Described in [cite:@Lec19a]
     - Personally, I prefer text over video when learning
       - Skim reading with superior search, navigation, and
         hyperlinks; own speed
       - Lots of students like audio explanations (and PDF), though
   - Education should be free and open
     - [[https://electures.uni-muenster.de/engage/theodul/ui/core.html?id=bfd84252-634d-40d0-996a-3979a21abe3e][Recording of a talk “Open Educational Resources: What, why, and how?”]]
     - Proper license attribution is a hassle
       - Emacs-reveal simplifies that process (for me), see [cite:@Lec19b]

** Offline work
   - Students often ask for download-able presentations
   - Alternatives
     1. Clone repository, build presentations locally (see [[#usage][Usage]])
     2. Download build artifacts from
        [[https://gitlab.com/oer/emacs-reveal-howto/-/pipelines][recent pipeline]]
        (if not expired)
     3. Generate PDF
        - Why, really?
	  - Why not download source files instead?
	  - [[https://orgmode.org/][Org mode]], which is plain text
	- Change the URL by adding “?print-pdf” after “.html”,
	  then print to PDF file (usually, Ctrl-p)
          - Or print to PDF in Docker
            - E.g.,
              [[https://oer.gitlab.io/emacs-reveal-howto/pdfs/How%20to%20create%20presentations%20with%20emacs-reveal.pdf][printed howto]]
	- Alternatively, generate PDF via LaTeX from Org
	  source file
          - Replace ~.html~ (and whatever follows) in address bar of
            browser with ~.pdf~
            - E.g.,
              [[https://oer.gitlab.io/emacs-reveal-howto/howto.pdf][this howto as PDF]]

** Audio
   :PROPERTIES:
   :reveal_extra_attr: data-audio-src="./audio/Tours_-_01_-_Enthusiast.ogg"
   :END:
   - Audio should start automatically here (differently from emacs-reveal’s default)
     - Enthusiast by
       [[https://freemusicarchive.org/music/Tours/][Tours]]
       - Licensed under
  	 [[https://creativecommons.org/licenses/by/3.0/][Creative Commons Attribution 3.0 Unported (CC BY 3.0)]]
       - Converted to [[https://en.wikipedia.org/wiki/Ogg][free Ogg format]] with [[https://www.audacityteam.org/][Audacity]]
     - See [[https://github.com/rajgoel/reveal.js-plugins/tree/master/audio-slideshow#user-content-compatibility-and-known-issues][compatibility and known issues of the underlying audio plugin]]
       - [[https://www.mozilla.org/en-US/firefox/][Firefox]],
         which I recommend as browser in general
         ([[https://blogs.fsfe.org/jens.lechtenboerger/2015/06/09/three-steps-towards-more-privacy-on-the-net/][here in English]]
         and [[https://www.informationelle-selbstbestimmung-im-internet.de/][here in German]]),
         seems to work everywhere
     - Audio controls are shown at bottom left

** (Speaker) Notes
   # The simplest form of macro reveallicense() occurs on
   # [[#what][a slide below]].  On this slide and below, it shows an
   # inline image with vertically aligned, rotated license statement.
   # Here, the fourth argument is t, which indicates that shortened
   # license information should be displayed.  This is particularly useful
   # for CC0 (Public Domain) work, which does not require attribution
   # but where others might still want to know that it can be reused
   # under less restrictive conditions than the overall work.
   - Slides contain additional notes as plain text if you see the
     folder icon at the top right (as on this slide)
     {{{reveallicense("./figures/icons/folder_inbox.meta","10rh",nil,t)}}}
     - Either press “v” to see the “courseware view”
       or click on that icon or press “s” to see the “speaker notes view”
     - You need to allow pop-ups
       - If the pop-up window does not work, you may need to press “s”
         twice or close the pop-up window once
#+BEGIN_NOTES
These are sample notes
- Lists can be used here
- You can time your presentation
  - Maybe look at [[https://gitlab.com/lechten/talks-2018/blob/master/2018-04-24-Blockchain.org][one]] of my talks to see how to define timing
#+END_NOTES

** Text-To-Speech (TTS)
- Audio can be generated from speaker notes and used in a “video-mode”
- [[./tts-howto.html][Demo presentation]]

* Introduction

** What’s This?
   :PROPERTIES:
   :CUSTOM_ID: what
   :END:
   # Note the use of macro reveallicense() below to show an inline image
   # with vertically aligned, rotated license statement.
   # Alternatively, one could also use standard Org mode features and
   # replace the macro with a #+CAPTION, which would be displayed
   # underneath the image.
   # Again alternatively, later as part of slide figure-with-meta-data,
   # you can see the use of macro revealimg(), to display a
   # horizontally centered image with caption and license statement.
   # The immediately following slide describes the required format of
   # meta-data.
   # Both macros are defined in config.org of oer-reveal.
   - ~Emacs-reveal~ is [[https://fsfe.org/freesoftware/][free software]]
     to generate [[https://revealjs.com/][reveal.js]] presentations (slides with audio) from simple text
     files in [[https://orgmode.org/][Org mode]]
     {{{reveallicense("./figures/3d-man/board.meta","30rh")}}}
     - Benefits
       - For your audience
         - Self-contained presentations embedding audio
         - Usable on lots of (including mobile and offline) devices with
           just a browser
       - For you as producer
         - Separation of layout and contents
	   (similarly to, e.g., LaTeX)
         - Simple text format allows diff and merge for ease of collaboration

** Prerequisites
  - I suppose (and strongly recommend) that you use GNU/Linux
    ([[https://getgnulinux.org/switch_to_linux/try_or_install/][help on getting started]])
    - Actually, not much here is operating system specific
  - ~Emacs-reveal~ should really be used with the text editor [[https://www.gnu.org/software/emacs/][GNU Emacs]]
    - (You could try other editors and build presentations within
      GitLab, thanks to GitLab’s infrastructure)
      - (In fact, you do not need an editor at all but could edit
        presentations using a Web browser on ~GitLab.com~, e.g., with the
        [[https://gitlab.com/-/ide/project/oer/emacs-reveal-howto/edit/master/][Web IDE]]
        (requires login))

** Installation and Quickstart
   - ~Emacs-reveal~ builds upon Gnu Emacs with
     [[https://orgmode.org][Org mode]]
     - [[https://gitlab.com/oer/emacs-reveal][~Emacs-reveal~ is
       available as free software on GitLab]]
   - You also need Git
     - [[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git][Getting started]]
       - The [[https://git-scm.com/book/en/v2][Pro Git book]] is a great source in general
     - [[https://oer.gitlab.io/oer-courses/cacs/Git-Introduction.html][Git introduction as OER]]
       (created with ~emacs-reveal~)
   - And maybe more, see next slide

*** LaTeX and other dependencies
    - By default, emacs-reveal generates HTML presentations and PDF variants
      - PDF output requires a LaTeX installation
        - If missing, ~elisp/publish.el~ stops with an error,
          resulting in *broken* presentations
      - Add following to beginning of ~elisp/publish.el~ to
        generate only HTML
        #+begin_src emacs-lisp
(setq oer-reveal-publish-org-publishing-functions
      '(oer-reveal-publish-to-reveal))
        #+end_src
    - This howto also contains a [[#dot-example][DOT/Graphviz example]]
      - Dependencies of emacs-reveal specified in two Docker files
        - [[https://gitlab.com/oer/emacs-reveal/-/blob/master/docker/debian-emacs-tex/Dockerfile][docker/debian-emacs-tex/Dockerfile]]
        - [[https://gitlab.com/oer/emacs-reveal/-/blob/master/docker/emacs-reveal/Dockerfile][docker/emacs-reveal/Dockerfile]]


*** Initial Consideration
    - ~Emacs-reveal~ can manage bundled software
      - (Submodules for Lisp
        packages Org mode, org-re-reveal, org-re-reveal-citeproc,
        org-re-reveal-ref, oer-reveal
        as well as reveal.js with several plugins)
      - Default with customizable variable
        ~emacs-reveal-managed-install-p~ being ~t~
      - Variable ~oer-reveal-revealjs-version~ specifies the
        target version of reveal.js for emacs-reveal
    - Or, you manage those components yourself
      - Set ~emacs-reveal-managed-install-p~ to ~nil~
    - In any case, ~emacs-reveal~ changes values of other packages
      (~org-ref~, ~oer-reveal~) without warning

*** Managed install of ~emacs-reveal~
    :PROPERTIES:
    :CUSTOM_ID: git-install
    :END:
    - Install ~emacs-reveal~ in a directory of your choice
      1. Choose directory, e.g., =~/.emacs.d/elpa=, and clone software
         - =cd ~/.emacs.d/elpa=
         - ~git clone --recursive https://gitlab.com/oer/emacs-reveal.git~
           - (Option ~--recursive~ downloads submodules)
      2. Add following lines to =~/.emacs=
         - =(add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal")=
         - =(require 'emacs-reveal)=
      3. Restart Emacs (installation of ~org-ref~ or ~citeproc~ is
         offered, if necessary)

*** Alternative installation
    - You may prefer to manage submodules of ~emacs-reveal~
      yourself
      1. Choose directory and clone (without option ~--recursive~)
         - =cd ~/.emacs.d/elpa=
         - ~git clone https://gitlab.com/oer/emacs-reveal.git~
      2. Add following lines to =~/.emacs=
         - =(add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal")=
         - =(setq emacs-reveal-managed-install-p nil)=
           - Read doc string of ~emacs-reveal-managed-install-p~
         - =(require 'emacs-reveal)=
      3. (Now, subdirectories under ="~/.emacs.d/elpa/emacs-reveal"=
         remain empty)

*** Quickstart with ~emacs-reveal~
    :PROPERTIES:
    :CUSTOM_ID: git-howto
    :END:
    - E.g., generate this howto
      1. Install emacs-reveal (see previous two slides for alternatives)
      2. Choose directory for howto, clone it
         - ~git clone --recursive https://gitlab.com/oer/emacs-reveal-howto.git~
           - Option ~--recursive~ gets an embedded repository for figures
         - ~cd emacs-reveal-howto/~
      3. Generate the HTML presentation from Org source ~howto.org~
         - ~emacs --batch --load elisp/publish.el~
         - Publication code needs to be able to locate ~emacs-reveal.el~
           - Code in ~elisp/publish.el~ tries
             (a) =~/.emacs.d/elpa/emacs-reveal= (suggested on
             [[#git-install][earlier slide]]) and
             (b) sibling directory ~emacs-reveal~

*** Default Configuration
    :PROPERTIES:
    :CUSTOM_ID: default-configuration
    :END:
    - Package oer-reveal (included in emacs-reveal) ships the file
      ~org/config.org~
      - Meant to be included in source files of presentations
        for default configuration
        - Included at top of the source code of this howto
        - Please take a look

* Usage
  :PROPERTIES:
  :CUSTOM_ID: usage
  :END:

** Alternatives
   1. Create presentations locally on Command Line
   2. Create presentations in GNU Emacs
   3. Create presentations with [[https://www.docker.com/][Docker]]
      {{{reveallicense("./figures/logos/docker-horizontal.meta","12rh")}}}
      - [[https://gitlab.com/oer/emacs-reveal/container_registry][Docker image emacs-reveal]]
        - Similarly to previous alternative; necessary software bundled
        - See [[https://gitlab.com/oer/emacs-reveal/blob/master/README.md][README of emacs-reveal]]
        - [[https://oer.gitlab.io/oer-courses/vm-neuland/Docker.html][Introduction to Docker]],
          built with ~emacs-reveal~
   4. Create and publish presentations on [[https://about.gitlab.com/][GitLab]]
      {{{reveallicense("./figures/logos/GitLab-wm_no_bg.meta","12rh")}}}
      - Based on [[https://docs.gitlab.com/ce/ci/][GitLab Continuous Integration infrastructure]]
        and above Docker image

** Build Presentations on Command Line
   0. [@0] Install [[#git-install][emacs-reveal]] and [[#git-howto][howto]]
   1. Create Org file in directory ~emacs-reveal-howto~
      - See contained source file for this presentation, ~howto.org~
   2. Build presentations for files ending in ~.org~
      - (Except internal ones, see function ~oer-reveal-publish-all~)
      - ~emacs --batch --load elisp/publish.el~
        - Presentations are built in subdirectory ~public/~
   3. Open presentation in [[https://www.mozilla.org/en-US/firefox/][Firefox]]
      - E.g.: ~firefox public/howto.html~
   4. Optional: Copy ~public/~ to public web server

** Build Presentations in Emacs
   1. Generate HTML presentation for visited
      ~.org~ file using Org export functionality: @@html:<br>@@
      Press ~C-c C-e w b~ (export with oer-reveal)
      - This generates HTML file in current directory and opens it
	in default browser
      - For this to work
        1. Settings of ~emacs-reveal~ should be in effect
           (~emacs-reveal.el~ is loaded, e.g., with
           [[#git-install][step (2) above]])
        2. Necessary resources, in particular
	  ~reveal.js~, must be accessible in ~.org~ file’s
	  directory
           - I use
             ~emacs --batch --load elisp/publish.el~
             once to populate ~public/~, then create a symbolic link:

             ~ln -s public/reveal.js~
        3. For image grids, you may need:
           ~(setq oer-reveal-export-dir "./")~

*** Org-re-reveal and oer-reveal
    - Emacs-reveal embeds the packages org-re-reveal and oer-reveal
      - Package oer-reveal is an Org mode export backend (extending
        org-re-reveal)
        - Starting with oer-reveal 1.4.0, part of emacs-reveal 4.1.0
        - With key binding mentioned on previous slide
      - You can export with org-re-reveal (~C-c C-e v v~ and ~C-c C-e
        v b~) or oer-reveal (~C-c C-e w w~ and ~C-c C-e w b~)
        - With oer-reveal, additional reveal.js plugins are enabled by
          default
          - See customizable variables ~oer-reveal-plugins~ and
            ~oer-reveal-plugin-config~


** Build Presentations in Docker
   - [[https://gitlab.com/oer/emacs-reveal/container_registry][Emacs-reveal has a Docker image]]
     - Docker image bundles necessary software
       - [[https://oer.gitlab.io/oer-courses/vm-neuland/Docker.html][Introduction to Docker]]
     - Sample invocations in directory of this project

       #+begin_src sh
docker run --rm -it -v $PWD:/oer registry.gitlab.com/oer/emacs-reveal/emacs-reveal:9.12.0
cd oer
emacs --batch --load elisp/publish.el
       #+end_src
     - See
       [[https://gitlab.com/oer/emacs-reveal/blob/master/README.md][README of emacs-reveal]] for more details

** Build Presentations on GitLab
   1. Fork [[https://gitlab.com/oer/emacs-reveal-howto][emacs-reveal-howto]]
      on GitLab ([[https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html][fork documentation]])
      - ~git clone <the URL of YOUR GitLab project>~
   2. Create or update Org files in cloned directory
      - Push changes to your fork
   3. GitLab infrastructure picks up changes and publishes presentations as
      [[https://about.gitlab.com/stages-devops-lifecycle/pages/][GitLab Pages]]
      - Based on Continuous Integration (CI) infrastructure
        - Configured by file
          [[https://gitlab.com/oer/emacs-reveal-howto/blob/master/.gitlab-ci.yml][.gitlab-ci.yml]]
      - CI run takes some minutes
      - Go to Settings → Pages to see the Pages’ address

* Some Presentation Features
  :PROPERTIES:
  :CUSTOM_ID: features
  :END:

** Text Slide
   - A list
   - With a sub-list whose items appear
     #+ATTR_REVEAL: :frag (appear)
     - This is /emphasized/
     - This is *bold*
     - This ~looks like code~
     - This is [[color:green][green]]
     - Nothing special

** Some Fragment Styles
   #+ATTR_REVEAL: :frag (gray-out shrink grow highlight-red)
   - Forget
   - Shrink
   - Grow
   - Very important

*** Fragments with Custom Order
    #+ATTR_REVEAL: :frag (appear) :frag_idx (1 4 3 2 1)
    * I’m first.
    * Fourth.
    * Third.
    * Second.
    * I’m also first.

** Centered Text

   #+ATTR_HTML: :class org-center
   Just some horizontally centered text.  Created by assigning class
   ~org-center~ (for which ~oer-reveal.css~ specifies ~text-align: center~).

   #+BEGIN_CENTER
   Alternatively, Org’s ~center~ blocks are exported by plain HTML export,
   see ~org-html-center-block~.
   #+END_CENTER

** On Sections
   # As explained in the Org manual, link targets can have different
   # forms.  The first link below points to #features, which has been
   # defined above as CUSTOM_ID.  The second link uses the section
   # header’s text as link target.
   - This slide is part of section [[#features][Some Presentation Features]]
     - We can link to slides, e.g., [[Text Slide][an earlier slide]]
       - You can use the browser history to go back
     - Side note: Check source code to see two variants of link
       targets used on this slide
   # The following directive with “appear” lets the next thing
   # appear in its entirety; instead of each list item individually as
   # on the previous slide with “(appear)” in parentheses.
   #+ATTR_REVEAL: :frag appear
   - This slide can also be perceived as its own subsection
     - The [[#another-anchor][next slide]] is on a deeper level of nesting
   - (This list item appears simultaneously with previous bullet point)

*** Another Slide
    :PROPERTIES:
    :CUSTOM_ID: another-anchor
    :END:
    - This slide is on a deeper level of nesting
    - This level of nesting is not shown in the table of contents in
      the slide’s bottom
    - By the way, the headings in the table of contents below are
      hyperlinks
      - And your browser remembers the history, back/forward buttons
        and shortcuts should work
      - Mousewheel and swiping work

** Licensing
   :PROPERTIES:
   :CUSTOM_ID: licensing
   :END:
   - Starting with emacs-reveal 5.0.3 (and oer-reveal 2.0.2),
     presentations can show license information derived from
     SPDX headers of the [[https://reuse.software/][REUSE]] project
     - See [[#license][licensing slide]] at the end of this presentation
       - Information on that slide is derived from
         header lines of ~howto.org~
         #+BEGIN_SRC
,#+SPDX-FileCopyrightText: 2017-2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
,#+SPDX-License-Identifier: CC-BY-SA-4.0
         #+END_SRC
       - Note that SPDX headers must be prefixed with ~#+~ to
         be recognized as Org mode keywords
     - License information is also embedded in machine-readable RDFa
       format
   - Macros for OER figures with (human- and machine-readable)
     license information are discussed [[#figure-with-meta-data][later]]

** Two Columns: Pro/Con of emacs-reveal
   #+ATTR_REVEAL: :frag appear
   #+BEGIN_leftcol
   Pro
   #+ATTR_REVEAL: :frag (appear)
   - Free/libre open source software
   - Device-independent presentations
     - Also mobile and offline
     - Generated from simple text format
       - Easy to learn
       - Collaboration with diff/merge/git
       - Separation of layout and content
   #+END_leftcol
   #+ATTR_REVEAL: :frag appear
   #+BEGIN_rightcol
   Con
   #+ATTR_REVEAL: :frag (appear)
   - No [[https://en.wikipedia.org/wiki/WYSIWYG][WYSIWYG]]
   - (Need to learn something new)
   #+END_rightcol

** Hyperlinks
   :PROPERTIES:
   :CUSTOM_ID: hyperlinks
   :END:
   - Different types of hyperlinks exist
     - External ones
       - Plain [[https://orgmode.org/][Org mode]] link
         - Or
           [[basic:https://orgmode.org/][with emphasis that you should really check out Org mode]]
           before you continue
       - Details of [[beyond:https://oer.gitlab.io/oer-courses/vm-neuland/Docker.html][Docker]]
         are beyond the scope of this howto
     - Internal ones (within presentation)
       - Maybe [[#licensing][pointing to an earlier slide]]
       - Or [[#url-parameters][pointing to a later slide]]
       - Or emphasizing that a mentioned concept like
         [[revisit:#figures-and-audio][figures and audio]] is revisited later

** URL Parameters
   :PROPERTIES:
   :CUSTOM_ID: url-parameters
   :END:
   - See [[https://oer.gitlab.io/hints.html][usage hints for emacs-reveal presentations]], e.g.:
     - [[./howto.html?default-navigation]] switches to the
       [[beyond:https://revealjs.com/vertical-slides/#navigation-mode][default navigation mode]]
        of reveal.js
     - [[./howto.html?hidelinks=32]] hides hyperlinks that go beyond
       presentation topics
       - (Note the link for navigation modes of reveal.js above)
       - Or both: [[./howto.html?default-navigation&hidelinks=32]]
     - Configure audio: ~audio-autoplay~, ~audio-speed=2.0~

* Figures and Audio
  :PROPERTIES:
  :CUSTOM_ID: figures-and-audio
  :END:
  - The following figures and their license metadata are maintained in
    a [[https://gitlab.com/oer/figures][separate project]]
    - Embedded here as Git submodule
    - See [[https://gitlab.com/oer/emacs-reveal-howto/-/blob/master/howto.org][source file]]
      for use of macros ~reveallicense~, ~revealimg~, ~revealgrid~
      - Macros defined and documented in
        [[https://gitlab.com/oer/oer-reveal/-/blob/master/org/config.org][config.org of oer-reveal]]
    - Presentation contains license information in machine-readable
      RDFa format [cite:@Lec19b]

** Slide with Figure and Audio
   :PROPERTIES:
   :reveal_extra_attr: data-audio-src="./audio/Tours_-_01_-_Enthusiast.ogg"
   :END:
   - This figure is part of a
     [[https://oer.gitlab.io/OS/Operating-Systems-Memory-II.html][different presentation]]
     {{{reveallicense("./figures/OS/clock-steps.meta","40rh",nil,'none)}}}
     - Notice: No license displayed for figure → License of document applies
   - The song Enthusiast by
     [[https://freemusicarchive.org/music/Tours/][Tours]]
     is licensed under
     [[https://creativecommons.org/licenses/by/3.0/][Creative Commons Attribution 3.0 Unported (CC BY 3.0)]]


** Figure with Caption and License
   :PROPERTIES:
   :CUSTOM_ID: figure-with-meta-data
   :END:
   - Display image with meta-data specified in file
     - Simplify sharing of images with source and license
   - Functionality and meta-data format are specific
     to ~emacs-reveal~
     - See next slide for sample file

   {{{revealimg("./figures/3d-man/decision.meta","To share or not to share","30rh")}}}

*** Meta-Data File for Previous Image
    :PROPERTIES:
    :CUSTOM_ID: sample-figure-meta-data
    :END:

#+INCLUDE: "./figures/3d-man/decision.meta" src emacs-lisp

** An Image Grid: Computers

{{{revealgrid(42,"./figures/devices/computer.grid",60,4,3,"\"ga1 ga2 ga2 ga3\" \"ga1 ga4 ga5 ga6\" \"ga7 ga8 ga9 ga9\"")}}}

*** Creation of Previous Image Grid
    - Single line in source file, using macro ~revealgrid~
      #+ATTR_REVEAL: :htmlize t
      #+BEGIN_SRC
{{{revealgrid(42,"./figures/devices/computer.grid",60,4,3,"\"ga1 ga2 ga2 ga3\" \"ga1 ga4 ga5 ga6\" \"ga7 ga8 ga9 ga9\"")}}}
      #+END_SRC
      - Arguments explained in
        [[https://gitlab.com/oer/oer-reveal/blob/master/org/config.org][config.org of oer-reveal]]
      - With file ~computer.grid~ as follows
        #+INCLUDE: "./figures/devices/computer.grid" src emacs-lisp

*** Notes on figures
    - If you used emacs-reveal previously and did not like that it
      exported all figures from a growing repository, note
      that as of emacs-reveal 5.2.0, only used figures are exported
    - So far, emacs-reveal uses meta-data in an ad-hoc format (as
      shown on a [[#sample-figure-meta-data][previous slide]])
      - For all [[https://gitlab.com/oer/figures/][figures in this repository]]
      - Please, contact me if you’d like to contribute with a
        different format, e.g., JSON-LD
        - Maybe with an [[https://gitlab.com/oer/figures/-/issues][issue]]?

** Appearing Items with Audio
   (Audios produced with
   [[https://github.com/marytts/marytts][MaryTTS]],
   converted to Ogg format with [[https://www.audacityteam.org/][Audacity]])

   #+ATTR_REVEAL: :frag (appear) :audio (./audio/1.ogg ./audio/2.ogg ./audio/3.ogg)
   - One
   - Two
   - Three

* Misc
** Quiz Plugin
   :PROPERTIES:
   :CUSTOM_ID: quiz-support
   :END:
   - ~Emacs-reveal~ embeds this
     [[https://gitlab.com/schaepermeier/reveal.js-quiz][quiz plugin]]
     - [[https://schaepermeier.gitlab.io/reveal-quiz-demo/demo.html][Demo of plugin’s author]]
   - In presentations, quizzes support active learning
     - In particular, retrieval practice

*** Sample Quiz
#+REVEAL_HTML: <script data-quiz src="./quizzes/sample-quiz.js"></script>

#+MACRO: klipse-languages (eval (message "%s" (mapconcat #'identity org-re-reveal-klipse-languages ", ")))
** Klipse for Code Evaluation
   :PROPERTIES:
   :CUSTOM_ID: klipse-support
   :END:
   - Org-re-reveal supports [[https://github.com/viebel/klipse][Klipse]]
     - Teach programming
       - Code changes in upper part result in output changes in lower part
     - Browser-side code evaluation for various programming languages
       - See ~org-re-reveal-klipse-languages~ for supported subset
         - {{{klipse-languages}}}
       - To activate, either add option ~reveal_klipsify_src:t~ (as in
         header of this file) or set variable ~org-re-reveal-klipsify-src~
         to ~t~; be sure to disable scaling of reveal.js
       - Correct indentation may require that you set
         ~org-src-preserve-indentation~ to ~t~ (see bottom of this file)
   - Code on next two slides copied from
     [[https://github.com/yjwen/org-reveal/blob/master/Readme.org][README of Org-Reveal]]

*** HTML Src Block
#+BEGIN_SRC html
<h1 class="whatever">hello, what's your name</h1>
#+END_SRC

*** Javascript Src Block
#+BEGIN_SRC js
console.log("success");
var x='string using single quote';
x
#+END_SRC

*** Python Src Block
#+BEGIN_SRC python
def factorial(n):
    if n < 2:
        return 1
    else:
        return n * factorial(n - 1)

print(factorial(10))
#+END_SRC

** Figures with Babel
   - Org export can execute embedded source code, with results
     injected into exported HTML presentation
     - For example, [[https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-dot.html][diagrams generated with ~dot~ of Graphviz]]
   - With emacs-reveal
     - Activate necessary source languages in ~oer-reveal-publish-babel-languages~
     - Maybe generate figures into separate directory
       - Publish contents with ~org-publish-project-alist~
   - See subsequent slides for sample code

*** Hello World with Dot
   :PROPERTIES:
   :CUSTOM_ID: dot-example
   :END:

#+ATTR_REVEAL: :htmlize t
#+BEGIN_SRC dot :file img/hello-world.png :exports both
graph {
  hello [label="Hello"];
  world [label="World!"];

  hello -- world;
  }
#+END_SRC

#+RESULTS:
[[file:img/hello-world.png]]

*** Relevant Excerpt of Publication Code
    - The following snippet of ~elisp/publish.el~ activates ~dot~ and
      publication of generated images
      - Adapt based on your needs
        - Note that necessary directories must exist (Babel does not
          create them)
      #+ATTR_REVEAL: :htmlize t
      #+begin_src elisp
(make-directory "img" t)
(setq oer-reveal-publish-babel-languages '((dot . t) (emacs-lisp . t))
      org-publish-project-alist
      (list (list "img"
                  :base-directory "img"
                  :base-extension "png"
                  :publishing-function 'org-publish-attachment
                  :publishing-directory "./public/img")))
      #+end_src

** Need Additional Software in Publication Process?
   - Maybe suggest as [[https://gitlab.com/oer/emacs-reveal/-/issues][issue for Docker image of emacs-reveal]]
   - Or install additional software in Docker container of your project with [[https://docs.gitlab.com/ee/ci/quick_start/][~before_script~]]

* The End
** Further Reading
   - [[https://orgmode.org/quickstart.html][Quickstart for Org mode]]
   - [[https://oer.gitlab.io/OS/][Presentations for a course on Operating Systems]]
     - My first use case for emacs-reveal
     - More features of Org mode (e.g., table of contents as agenda,
       keyword index) and reveal.js (e.g., notes, animated SVGs)

** Go for it!

   {{{revealimg("./figures/3d-man/steps.meta","The road ahead …")}}}

   [[https://gitlab.com/oer/]]

** Bibliography
   :PROPERTIES:
   :CUSTOM_ID: bibliography
   :END:

#+print_bibliography:

#+MACRO: licensepreamble
#+INCLUDE: "~/.emacs.d/oer-reveal-org/license-template.org" :minlevel 2
#+INCLUDE: "~/.emacs.d/oer-reveal-org/license-disclaimer-cc-by-sa-4.0.org"

# Local Variables:
# indent-tabs-mode: nil
# org-src-preserve-indentation: t
# End:
